#ifndef HOUSE_MESH_H
#define HOUSE_MESH_H

#include <string>
#include <vector>
#include "Face.h"


class Mesh {
public:
    explicit Mesh(std::string obj_name);

    void add(const Face& face);

    std::string toObjString();

private:
    static uint64_t vertexHash(Vertex vertex);

    void joinFacesVertices();

    void preCalculateUVs();

    void separateFacesBySmoothness();

    static std::string vertexToObjString(const std::string& prefix, Vertex vertex);

    std::string faceToObjString(uint64_t face_idx);

    static std::string joinWithSlashes(std::vector<std::string> parts);

private:
    std::string header;
    std::string name;
    std::vector<Face> faces;

    std::vector<std::vector<uint64_t>> face_vertices_idx;
    std::vector<std::vector<uint64_t>> face_uvs_idx;

    std::vector<Vertex> unique_vertices;
    std::vector<Vertex> uvs;

    std::vector<uint64_t> smooth_faces_idx;
    std::vector<uint64_t> flat_faces_idx;
};


#endif //HOUSE_MESH_H
