#ifndef HOUSE_HOUSEFLOOR_H
#define HOUSE_HOUSEFLOOR_H

#include <utility>
#include <optional>
#include <vector>
#include <memory>
#include "Vertex.h"
#include "ObjParser.h"
#include "mesh_obj/Mesh.h"

using std::vector;
using std::optional;

class HouseFloor {
public:
    struct WallRoofConfig {
        bool has_roof = false;
        float height;
        float cut_height;
        float cut_angle;
    };
    struct RoofConfig {
        WallRoofConfig left;
        WallRoofConfig top;
        WallRoofConfig right;
        WallRoofConfig bottom;
    };
    struct SkeletonLine {
        float len;
        float width;
        float rotation;
        Vertex start;
        RoofConfig roof;
    };
    enum Side {
        SD_LEFT, SD_TOP, SD_RIGHT, SD_BOTTOM
    };
public:
    explicit HouseFloor(float height, float start = 0);

    /* Skeleton */
    void addLine(SkeletonLine line);

    void addLine(Vertex start, float len, float slope, float width) {
        RoofConfig conf;
        conf.left.has_roof = false;
        conf.top.has_roof = false;
        conf.right.has_roof = false;
        conf.bottom.has_roof = false;
        addLine(start, len, slope, width, conf);
    };

    void addLine(Vertex start, float len, float slope, float width, RoofConfig roof_conf);

    void generateWalls();

    Polygon getFoundation() { return foundation_poly; }

    void addToMesh(Mesh *obj);

protected:
    static Polygon getLineRectangle(SkeletonLine line);

    void generateAllRoofs();

    void generateRoof(SkeletonLine line, const Polygon &rect);

    Polygon rectangularRamp(SkeletonLine line, const Polygon &rect, Side side, float len_mult);

    Polygon triangularRamp(SkeletonLine line, const Polygon &rect, Side side, float len_mult);

private:
    vector<SkeletonLine> skeleton_lines;

    vector<Polygon> skeleton_line_rectangles;

    float floor_height;

    float floor_start;

    Polygon foundation_poly = {};

    vector<Polygon> roof = {};
};


#endif //HOUSE_HOUSEFLOOR_H
