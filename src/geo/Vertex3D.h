#ifndef HOUSE_VERTEX3D_H
#define HOUSE_VERTEX3D_H

#include "Vector2D.h"

class Vertex3D {
public:
    Vertex3D(double px, double py, double pz);

    Vertex3D operator+(const Vertex3D &vertex);

    Vertex3D operator-(const Vertex3D &vertex);

    Vertex3D operator/(double scalar);

    static bool is3D(){ return false;}

public:
    double x;
    double y;

protected:
    double z;
};


#endif //HOUSE_VERTEX3D_H
