#include "HouseFloor.h"
#include "common.h"
#include "Vector2D.h"
#include "mesh_obj/Face.h"

HouseFloor::HouseFloor(float height, float start) {
    floor_height = height;
    floor_start = start;
}

void HouseFloor::addLine(SkeletonLine line) {
    skeleton_lines.push_back(line);
}

void HouseFloor::addLine(Vertex start, float len, float rotation, float width, RoofConfig roof_conf) {
    SkeletonLine line = {len, width, rotation, start, roof_conf};
    return addLine(line);
}


void HouseFloor::generateWalls() {
    if (!foundation_poly.empty()) return;

    for (const auto &line: skeleton_lines) {
        skeleton_line_rectangles.push_back(getLineRectangle(line));
    }
    foundation_poly = joinPolygons(skeleton_line_rectangles);

    generateAllRoofs();
}

Polygon HouseFloor::getLineRectangle(SkeletonLine line) {
    Vector2D perpendicular_vector(getPerpendicularAngle(line.rotation), line.width);
    Vertex end = line.start + Vector2D(line.rotation, line.len);
    Vertex start = line.start;
    return {
            start + perpendicular_vector,
            end + perpendicular_vector,
            end - perpendicular_vector,
            start - perpendicular_vector,
    };
}

void HouseFloor::addToMesh(Mesh *mesh) {
    Vertex start_vertex = Vertex(0, 0, floor_start);
    float final_height = floor_height;

    if (foundation_poly.empty()) generateWalls();
    for (auto pt2 = begin(foundation_poly) + 1; pt2 != end(foundation_poly); ++pt2) {
        auto pt1 = pt2 - 1;
        Face wall = Face::verticalRectangle(*pt1 + start_vertex, *pt2 + start_vertex, final_height);
        mesh->add(wall);
    }
    Face wall = Face::verticalRectangle(*(foundation_poly.end() - 1) + start_vertex, foundation_poly[0] + start_vertex, final_height);
    mesh->add(wall);
    for (const Polygon &poly: roof) {
        if(poly.size() == 3) mesh->add(Face(poly[0], poly[1], poly[2]));
        else if(poly.size() == 4) mesh->add(Face(poly[0], poly[1], poly[2], poly[3]));
    }
}

void HouseFloor::generateAllRoofs() {
    if (foundation_poly.empty()) generateWalls();
    uint64_t i = 0;
    for (const auto &line: skeleton_lines) {
        generateRoof(line, skeleton_line_rectangles[i]);
        i++;
    }
}

void HouseFloor::generateRoof(SkeletonLine line, const Polygon &rect) {
    bool left = line.roof.left.has_roof;
    bool top = line.roof.top.has_roof;
    bool right = line.roof.right.has_roof;
    bool bottom = line.roof.bottom.has_roof;
    uint8_t num_roofs = left + top + right + bottom;
    if (num_roofs == 0) return;
    bool opposite_sides = (left && right) || (top && bottom);

    /* Single side full slope */
    if (num_roofs == 1) {
        Side side;
        if (left) side = SD_LEFT;
        else if (top) side = SD_TOP;
        else if (right) side = SD_RIGHT;
        else side = SD_BOTTOM;
        Polygon ramp = rectangularRamp(line, rect, side, 1.0f);
        if (!ramp.empty()) roof.push_back(ramp);
    }
    /* Two opposing sides halfway */
    else if (num_roofs == 2 && opposite_sides) {
        Side side1 = left ? SD_LEFT : SD_TOP;
        Side side2 = left ? SD_TOP : SD_BOTTOM;
        Polygon ramp1 = rectangularRamp(line, rect, side1, 0.5f);
        Polygon ramp2 = rectangularRamp(line, rect, side2, 0.5f);
        if (!ramp1.empty() && !ramp2.empty()) {
            roof.push_back(ramp1);
            roof.push_back(ramp2);
        }
    }
    /* All but on side */
    else if (num_roofs == 3) {
        vector<Side> sides;
        if (!left) sides = {SD_TOP, SD_RIGHT, SD_BOTTOM};
        else if (!top) sides = {SD_LEFT, SD_RIGHT, SD_BOTTOM};
        else if (!right) sides = {SD_LEFT, SD_TOP, SD_BOTTOM};
        else if (!bottom) sides = {SD_LEFT, SD_TOP, SD_RIGHT};
        Polygon ramp1 = triangularRamp(line, rect, sides[0], 1.0f);
        Polygon ramp2 = triangularRamp(line, rect, sides[1], 1.0f);
        Polygon ramp3 = triangularRamp(line, rect, sides[2], 1.0f);
        if (!ramp1.empty() && !ramp2.empty() && !ramp3.empty()) {
//            roof.push_back(ramp1);
            roof.push_back(ramp2);
//            roof.push_back(ramp3);
        }
    }
    /* All sides */
    else if (num_roofs == 4) {
        Polygon ramp1 = triangularRamp(line, rect, SD_RIGHT, 0.5f);
        Polygon ramp2 = triangularRamp(line, rect, SD_TOP, 0.5f);
        Polygon ramp3 = triangularRamp(line, rect, SD_LEFT, 0.5f);
        Polygon ramp4 = triangularRamp(line, rect, SD_BOTTOM, 0.5f);
        if (!ramp1.empty() && !ramp2.empty() && !ramp3.empty() && !ramp4.empty()) {
            roof.push_back(ramp1);
            roof.push_back(ramp2);
            roof.push_back(ramp3);
            roof.push_back(ramp4);
        }
    }
}

Polygon HouseFloor::rectangularRamp(SkeletonLine line, const Polygon &rect, Side side, float len_mult) {
    Polygon roof_line;
    float roof_height;
    float length;
    float direction;
    float final_height = floor_height + floor_start;

    if (side == SD_LEFT) {
        roof_line = {rect[0].withZ(final_height), rect[3].withZ(final_height)};
        roof_height = line.roof.left.height;
        length = line.len * len_mult;
        direction = increaseAngleDeg(line.rotation, 0.0f);
    } else if (side == SD_RIGHT) {
        roof_line = {rect[1].withZ(final_height), rect[2].withZ(final_height)};
        roof_height = line.roof.right.height;
        length = line.len * len_mult;
        direction = increaseAngleDeg(line.rotation, 180.0f);
    } else if (side == SD_TOP) {
        roof_line = {rect[0].withZ(final_height), rect[1].withZ(final_height)};
        roof_height = line.roof.top.height;
        length = 2 * line.width * len_mult;
        direction = increaseAngleDeg(line.rotation, 270.0f);
    } else if (side == SD_BOTTOM) {
        roof_line = {rect[2].withZ(final_height), rect[3].withZ(final_height)};
        roof_height = line.roof.bottom.height;
        length = 2 * line.width * len_mult;
        direction = increaseAngleDeg(line.rotation, 90.0f);
    } else {
        return {};
    }

    Vertex slope_end_vertex = Vertex(0, 0, roof_height) + Vector2D(direction, length);
    return {roof_line[0], roof_line[0] + slope_end_vertex, roof_line[1] + slope_end_vertex, roof_line[1]};
}

Polygon HouseFloor::triangularRamp(SkeletonLine line, const Polygon &rect, HouseFloor::Side side, float len_mult) {
    Polygon roof_line;
    float roof_height;
    float length;
    float direction;
    float final_height = floor_height + floor_start;

    if (side == SD_LEFT) {
        roof_line = {rect[0].withZ(final_height), rect[3].withZ(final_height)};
        roof_height = line.roof.left.height;
        length = line.len * len_mult;
        direction = increaseAngleDeg(line.rotation, 0.0f);
    } else if (side == SD_RIGHT) {
        roof_line = {rect[1].withZ(final_height), rect[2].withZ(final_height)};
        roof_height = line.roof.right.height;
        length = line.len * len_mult;
        direction = increaseAngleDeg(line.rotation, 180.0f);
    } else if (side == SD_TOP) {
        roof_line = {rect[0].withZ(final_height), rect[1].withZ(final_height)};
        roof_height = line.roof.top.height;
        length = 2 * line.width * len_mult;
        direction = increaseAngleDeg(line.rotation, 270.0f);
    } else if (side == SD_BOTTOM) {
        roof_line = {rect[2].withZ(final_height), rect[3].withZ(final_height)};
        roof_height = line.roof.bottom.height;
        length = 2 * line.width * len_mult;
        direction = increaseAngleDeg(line.rotation, 90.0f);
    } else {
        return {};
    }
    Vertex midpt = ((roof_line[1] + roof_line[0]) / 2).withZ(roof_line[0].z);
    Vertex slope_end_vertex = Vertex(0, 0, roof_height) + Vector2D(direction, length);
    return {roof_line[0], midpt + slope_end_vertex, roof_line[1]};
}






