#include <iostream>
#include <iomanip>
#include "src/HouseFloor.h"
#include "src/mesh_obj/Mesh.h"
//#include "src/ObjParser.h"
#include "src/texture/roof/TileRoof.h"

int main() {
    std::cout << std::fixed;
    std::cout << std::setprecision(2);

    Mesh mesh("test");

    HouseFloor::RoofConfig roof1;
    roof1.right.has_roof = true;
    roof1.right.height = 1;
    roof1.right.cut_height = 0;

    HouseFloor::RoofConfig roof2;
    roof2.top.has_roof = true;
    roof2.top.height = 2.5;
    roof2.top.cut_height = 0;
    roof2.bottom.has_roof = true;
    roof2.bottom.height = 2.5;
    roof2.bottom.cut_height = 0;

    HouseFloor::RoofConfig roof4;
    roof4.top.has_roof = true;
    roof4.top.height = 2;
    roof4.top.cut_height = 0;
    roof4.bottom.has_roof = true;
    roof4.bottom.height = 2;
    roof4.bottom.cut_height = 0;
    roof4.left.has_roof = true;
    roof4.left.height = 2;
    roof4.left.cut_height = 0;
    roof4.right.has_roof = true;
    roof4.right.height = 2;
    roof4.right.cut_height = 0;

    HouseFloor floor_1(3.2f);
    floor_1.addLine({0, 0}, 10.0f, 0, 3.5f, roof2);
    floor_1.addLine({3.5, -2.0}, 3.5, -1.5707, 2, roof1);
    floor_1.addLine({9, 0}, 3, 0, 2, roof1);
    floor_1.addToMesh(&mesh);

//    HouseFloor floor_1_2(2.9f);
//    floor_1_2.addLine({-2, 0}, 4.0f, 0, 3.0f, roof2);
//    floor_1_2.addToMesh(&mesh);

    HouseFloor floor_2(3.2f, 3.2f);
    floor_2.addLine({5, 0}, 4.0f, 0, 3.0f, roof4);
    floor_2.addToMesh(&mesh);

    std::cout << mesh.toObjString();

    TileRoof roof_texture(700, 400);
    roof_texture.visualize();

    return 0;
}
//
//#include <iostream>
//#include <iomanip>
//#include "src/mesh_obj/Mesh.h"
//
//int main() {
//    Mesh mesh("test");
//    mesh.add(Face({
//        Vertex(0.0, 0.0, 0.0),
//        Vertex(0.0, 10.0, 0.0),
//        Vertex(0.0, 10.0, 10.0),
//        Vertex(0.0, 0.0, 10.0),
//    }));
//    mesh.add(Face({
//        Vertex(0.0, 0.0, 0.0),
//        Vertex(10.0, 0.0, 0.0),
//        Vertex(10.0, 0.0, 10.0),
//        Vertex(0.0, 0.0, 10.0),
//    }));
//    std::cout << mesh.toObjString();
//    return 0;
//}
//
