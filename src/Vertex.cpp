#include <math.h>
#include "Vertex.h"
#include "Vector2D.h"

Vertex Vertex::operator+(const Vector2D &vector) {
    return {
            x + cos(vector.direction) * vector.magnitude,
            y + sin(vector.direction) * vector.magnitude,
            z
    };
}

Vertex Vertex::operator-(const Vector2D &vector) {
    return {
            x - cos(vector.direction) * vector.magnitude,
            y - sin(vector.direction) * vector.magnitude,
            z
    };
}

Vertex Vertex::operator+(const Vertex &vertex) {
    if (vertex.is_3d || is_3d) {
        return {x + vertex.x, y + vertex.y, z + vertex.z};
    }
    return {x + vertex.x, y + vertex.y};
}

Vertex Vertex::operator/(float scalar) {
    return {
            x / scalar,
            y  / scalar,
            z / scalar,
    };
}
