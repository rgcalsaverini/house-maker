#include "Vertex3D.h"

Vertex3D::Vertex3D(double px, double py, double pz) : x(px), y(py), z(pz) {}


Vertex3D Vertex3D::operator+(const Vertex3D &vertex) {
    return {
            x + vertex.x,
            y + vertex.y,
            z + vertex.z,
    };
}

Vertex3D Vertex3D::operator-(const Vertex3D &vertex) {
    return {
            x - vertex.x,
            y - vertex.y,
            z - vertex.z,
    };
}

Vertex3D Vertex3D::operator/(double scalar) {
    return {
            x / scalar,
            y / scalar,
            z / scalar,
    };
}
