#ifndef HOUSE_FACE_H
#define HOUSE_FACE_H


#include <vector>
#include "../Vertex.h"

class Face {
public:
    // Constructs a triangular face
    Face(Vertex p1, Vertex p2, Vertex p3);

    // Constructs a rectangular face
    Face(Vertex p1, Vertex p2, Vertex p3, Vertex p4);

    // Factory for faces built from a vertically extruded 2-point edge
    static Face verticalRectangle(Vertex base1, Vertex base2, float height);

    [[nodiscard]] std::vector<Vertex> vertices() const;

    [[nodiscard]] std::vector<Vertex> uv() const;

//    std::pair<>() {}

    void shadeSmooth();

    void shadeFlat();

    [[nodiscard]] bool isSmooth() const;

    [[nodiscard]] bool isTriangular() const;

private:
    void addTriangleUV();

    void addRectangleUV();

private:
    // 3D vertices that compose the mesh. Can be a triangle or a rectangle
    std::vector<Vertex> geometry_points;
    // 2D vertices that compose the UV layout
    std::vector<Vertex> uv_points;
    // UV bounding rectangle (width x height)
    std::pair<float, float> uv_bounding_box;
    // Whether this face will be shaded as smooth
    bool smooth = false;
};


#endif //HOUSE_FACE_H
