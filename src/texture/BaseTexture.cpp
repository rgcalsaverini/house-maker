#include "BaseTexture.h"

BaseTexture::BaseTexture(int width, int height, float zoom, float aa) :
        final_width(width),
        final_height(height),
        img_zoom(zoom),
        anti_aliasing(aa) {

    img_width = ceil(width * aa);
    img_height = ceil(height * aa);

    std::random_device random_device;
    random_engine = std::mt19937(random_device());
}

BaseTexture::~BaseTexture() {
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
}

void BaseTexture::visualize() {
    if (!init()) {
        printf("Failed to initialize!\n");
    } else {

        //Main loop flag
        bool quit = false;

        //Event handler
        SDL_Event e;

        generateTexture();

        //While application is running
        while (!quit) {
            //Handle events on queue
            while (SDL_PollEvent(&e) != 0) {
                //User requests quit
                if (e.type == SDL_QUIT) {
                    quit = true;
                } else if( e.type == SDL_KEYDOWN ) {
                    if(e.key.keysym.sym == SDLK_SPACE) {
                        generateTexture();
                    } else if(e.key.keysym.sym == SDLK_ESCAPE || e.key.keysym.sym == SDLK_q) {
                        quit = true;
                    }
                }
            }

            //Update screen
            SDL_RenderPresent(renderer);
        }
    }

    //Free resources and close SDL
    close();
}

bool BaseTexture::init() {
    bool success = true;

    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
        success = false;
    } else {
        if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2")) {
            printf("Warning: Linear texture filtering not enabled!");
        }
        window = SDL_CreateWindow("Sample",
                                  SDL_WINDOWPOS_UNDEFINED,
                                  SDL_WINDOWPOS_UNDEFINED,
                                  img_width,
                                  img_height,
                                  SDL_WINDOW_SHOWN);
        if (window == NULL) {
            printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
            success = false;
        } else {
            //Create renderer for window
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if (renderer == NULL) {
                printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
                success = false;
            } else {
                SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
            }
        }
    }

    return success;
}

void BaseTexture::close() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    window = NULL;
    renderer = NULL;
    IMG_Quit();
    SDL_Quit();
}


// y gast ank
// Mr Peter Ky