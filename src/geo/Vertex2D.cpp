#include <cmath>
#include "Vertex2D.h"
#include "Vector2D.h"

Vertex2D::Vertex2D(double px, double py) : x(px), y(py) {}

Vertex2D Vertex2D::operator+(const Vector2D &vector) {
    return {
            x + cos(vector.direction) * vector.magnitude,
            y + sin(vector.direction) * vector.magnitude,
    };
}

Vertex2D Vertex2D::operator-(const Vector2D &vector) {
    return {
            x - cos(vector.direction) * vector.magnitude,
            y - sin(vector.direction) * vector.magnitude,
    };
}

Vertex2D Vertex2D::operator+(const Vertex2D &vertex) {
    return {x + vertex.x, y + vertex.y};
}

Vertex2D Vertex2D::operator/(double scalar) {
    return {
            x / scalar,
            y / scalar,
    };
}
