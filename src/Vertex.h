#ifndef HOUSE_VERTEX_H
#define HOUSE_VERTEX_H

#include <vector>

class Vector2D;

class Vertex {
public:
    Vertex(float px, float py, float pz) : x(px), y(py), z(pz), is_3d(true) {};

    Vertex(float px, float py) : x(px), y(py), z(0), is_3d(false) {};

    [[nodiscard]] Vertex withZ(float new_z) const { return Vertex(x, y, new_z); }

    Vertex operator+(const Vector2D &vector);

    Vertex operator+(const Vertex &vertex);

    Vertex operator-(const Vector2D &vector);

    Vertex operator/(float scalar);

public:
    float x;
    float y;
    float z;
    bool is_3d;
};

typedef std::vector<Vertex> Polygon;
#endif //HOUSE_VERTEX_H
