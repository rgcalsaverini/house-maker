#ifndef HOUSE_VERTEX2D_H
#define HOUSE_VERTEX2D_H

#include <vector>

class Vector2D;

class Vertex2D {
public:
    Vertex2D(double px, double py);

//    [[nodiscard]] Vertex3D withZ(float new_z) const { return Vertex(x, y, new_z); }

    Vertex2D operator+(const Vector2D &vector);

    Vertex2D operator+(const Vertex2D &vertex);

    Vertex2D operator-(const Vector2D &vector);

    Vertex2D operator/(double scalar);

    static bool is3D(){ return false;}

public:
    double x;
    double y;
};

typedef std::vector<Vertex2D> Polygon2D;

#endif //HOUSE_VERTEX2D_H
