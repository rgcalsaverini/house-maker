#include "TileRoof.h"
#include <cmath>
#include <SDL2_gfxPrimitives.h>

// renderer to texture:
// https://gist.github.com/Twinklebear/8265888

TileRoof::TileRoof(int width, int height, float zoom, float aa) : BaseTexture(width, height, zoom, aa) {
    cursor_x = 0;
}

void TileRoof::generateTexture() {
    std::uniform_real_distribution<float> rnd_cursor_x(-1.0, 1.0);
    std::normal_distribution<float> rnd_norm_radius(0.6, 0.2);
    std::normal_distribution<float> rnd_height(1.0, 0.05);
    std::normal_distribution<float> rnd_col(0.0, 3.0);
    Uint8 col[3] = {0x63, 0x54, 0x48};
//    std::normal_distribution<float> rnd_cursor_x(1.0, 0.1);
//    std::normal_distribution<float> rnd_norm_radius(0.3, 0.1);
//    std::normal_distribution<float> rnd_height(1.0, 0.05);
//    std::normal_distribution<float> rnd_col(0.0, 2.0);
//    Uint8 col[3] = {0x35, 0x3d, 0x37};

    SDL_SetRenderDrawColor(renderer, col[0], col[1], col[2], 0xFF);
    SDL_RenderClear(renderer);
    int num_cols = (int)(2 * finalSize(img_height)  / (finalSize(regular_height) * 0.6) + 2);

    cursor_x = 0;
    for (int i = 0; i < num_cols; i++) {
        bool row_ended = false;
        for (int j = -1; !row_ended; j++) {
            Uint8 r = col[0] + (Uint8) (rnd_col(random_engine));
            Uint8 g = col[1] + (Uint8) (rnd_col(random_engine));
            Uint8 b = col[2] + (Uint8) (rnd_col(random_engine));
            float radius = rnd_norm_radius(random_engine);
            radius = radius < 0 ? 0 : radius;
            radius = radius > 1 ? 1 : radius;
            float height = rnd_height(random_engine);
            row_ended = drawTile(i, j, height, radius, r, g, b);

        }
        cursor_x += (int) (rnd_cursor_x(random_engine) * (regular_width / 2.0));
        if (cursor_x >= regular_width) {
            cursor_x = 0;
        }
    }

//    drawTile(7, 8, 1.0, 1.0, 0x00, 0x55, 0xAA);
//    drawTile(7, 7, 1.0, 0.5, 0x88, 0x00, 0x00);
//    drawTile(7, 6, 1.0, 0.1, 0xff, 0xaa, 0x00);
//    drawTile(7, 5, 1.0, 0.0, 0x00, 0x88, 0x00);
}

bool TileRoof::drawTile(int row, int col, float height, float radius, Uint8 r, Uint8 g, Uint8 b) {
    Sint16 tile_width = finalSize(regular_width);
    Sint16 tile_height = finalSize(regular_height * height);
    Sint16 rad = radius * (tile_width / 2.0);
    auto dark_r = (Uint8) (r * 0.7);
    auto dark_g = (Uint8) (g * 0.7);
    auto dark_b = (Uint8) (b * 0.7);
    Sint16 x0 = col * finalSize(regular_width) + finalSize(cursor_x);
    Sint16 y0 = img_height - row * (Sint16) (finalSize(regular_height) * 0.6);
    Sint16 x1 = x0 + tile_width;
    Sint16 y1 = y0 + tile_height;
    Sint16 h0 = x0 + rad;
    Sint16 h1 = x1 - rad;
    Sint16 k = y1 - rad;

    boxRGBA(renderer, x0, y0, x1, k, r, g, b, 0xFF);
    boxRGBA(renderer, h0, k, h1, y1 - 1, r, g, b, 0xFF);
    filledCircleRGBA(renderer, h0, k, rad, r, g, b, 0xFF);
    filledCircleRGBA(renderer, h1, k, rad, r, g, b, 0xFF);

    lineRGBA(renderer, x0, y0, x0, k, dark_r, dark_g, dark_b, 0xFF);
    lineRGBA(renderer, x1, y0, x1, k, dark_r, dark_g, dark_b, 0xFF);
    lineRGBA(renderer, h0, y1, h1, y1, dark_r, dark_g, dark_b, 0xFF);
    arcRGBA(renderer, h0, k, rad, 90, 180, dark_r, dark_g, dark_b, 0xFF);
    arcRGBA(renderer, h1, k, rad, 0, -270, dark_r, dark_g, dark_b, 0xFF);

    Sint16 tile_thickness = finalSize(2);
    for (int x = x0; x < h0; x++) {
        double eq_b = -2.0 * k;
        double eq_c = k * k + (x - h0) * (x - h0) - (rad * rad);
        double root = k + std::sqrt(eq_b * eq_b - 4.0 * eq_c) / 2.0;
        auto y = (Sint16)(root - 1);
        Sint16 mirror_x = x1 - (x - x0);
        lineRGBA(renderer, x, y, x, y + tile_thickness, dark_r, dark_g, dark_b, 0xFF);
        lineRGBA(renderer, mirror_x, y, mirror_x, y + tile_thickness, dark_r, dark_g, dark_b, 0xFF);
    }
    boxRGBA(renderer, h0, y1 - 1, h1, y1 - 2 + tile_thickness, dark_r, dark_g, dark_b, 0xFF);

    int max = (y1 - y0) / 2;
    int i = max;
    while (i > 0) {
        Uint8 alpha = (float) (i) / (float) (max) * 0x88;
        i--;
        y0++;
        lineRGBA(renderer, x0, y0, x1, y0, r / 3, g / 3, b / 3, alpha);
    }
    return x1 > img_width;
}
