#ifndef HOUSE_BASETEXTURE_H
#define HOUSE_BASETEXTURE_H


#include <cmath>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL2_gfxPrimitives.h>
#include <random>

class BaseTexture {
public:
    BaseTexture(int width, int height, float zoom = 1.0f, float aa = 2.0f);

    ~BaseTexture();

    void visualize();

protected:
    virtual void generateTexture() {};

    int finalSize(int measure) { return (int) round(((float) (measure)) * img_zoom * anti_aliasing); }

    bool init();

    void close();

protected:
    int img_width;
    int img_height;
    int final_width;
    int final_height;
    float img_zoom;
    float anti_aliasing;
    SDL_Window *window;
    SDL_Renderer *renderer;
    std::mt19937 random_engine;
};


#endif //HOUSE_BASETEXTURE_H
