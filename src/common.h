#ifndef HOUSE_COMMON_H
#define HOUSE_COMMON_H

#include <limits>
#include <vector>
#include <cmath>
#include "clipper/clipper.h"

#define M_PI_FLOAT ((float)(M_PI))
#define M_PI_FLOAT_TIMES_2 ((float)(M_PI * 2))

float getPerpendicularAngle(float angle) {
    angle += M_PI_FLOAT / 2.0;
    return angle > M_PI_FLOAT_TIMES_2 ? angle - M_PI_FLOAT_TIMES_2 : angle;
}

float increaseAngleDeg(float angle_rad, float increment_deg) {
    float increment = (increment_deg / 180.0f) * M_PI_FLOAT;
    angle_rad += increment;
    while (angle_rad < 0) angle_rad += M_PI_FLOAT_TIMES_2;
    while (angle_rad > M_PI_FLOAT_TIMES_2) angle_rad += M_PI_FLOAT_TIMES_2;
    return angle_rad;
}

ClipperLib::Path polyToClipperPath(const Polygon &poly) {
    ClipperLib::Path path;
    for (const auto &vertex: poly) {
        path.push_back({static_cast<ClipperLib::cInt>((ClipperLib::cInt) 100 * vertex.x),
                        static_cast<ClipperLib::cInt>((ClipperLib::cInt) 100 * vertex.y)});
    }
    return path;
}

Polygon clipperPathToPoly(const ClipperLib::Path &path) {
    Polygon poly;
    for (const auto &pt: path) {
        poly.push_back({static_cast<float>((float) 0.01 * pt.X),
                        static_cast<float>((float) 0.01 * pt.Y)});
    }
    return poly;
}

Polygon joinPolygons(std::vector<Polygon> polygons) {
    ClipperLib::Clipper clipper;
    ClipperLib::Path path = polyToClipperPath(polygons[0]);
    clipper.AddPaths({path}, ClipperLib::ptSubject, true);
    auto end_lines = end(polygons);
    for (auto poly = begin(polygons) + 1; poly != end_lines; ++poly) {
        ClipperLib::Path new_path = polyToClipperPath(*poly);
        clipper.AddPaths({new_path}, ClipperLib::ptClip, true);
    }
    std::vector<ClipperLib::Path> solution;
    clipper.Execute(ClipperLib::ctUnion, solution, ClipperLib::pftNonZero, ClipperLib::pftNonZero);
    return clipperPathToPoly(solution[0]);
}





#endif //HOUSE_COMMON_H
