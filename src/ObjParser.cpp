#include <iostream>
#include "ObjParser.h"

using std::to_string;

void ObjParser::addVerticalPlane(Vertex base1, Vertex base2, float height) {
    Vertex pt1 = Vertex(base1.x, base1.y, base1.z);
    Vertex pt2 = Vertex(base2.x, base2.y, base1.z);
    Vertex pt3 = Vertex(pt2.x, pt2.y, height);
    Vertex pt4 = Vertex(pt1.x, pt1.y, height);

    uint64_t v1 = addVertex(pt1);
    uint64_t v2 = addVertex(pt2);
    uint64_t v3 = addVertex(pt3);
    uint64_t v4 = addVertex(pt4);

    faces.push_back(
            joinWithSlash({to_string(v1), "1", "1"}) + " " +
            joinWithSlash({to_string(v2), "2", "1"}) + " " +
            joinWithSlash({to_string(v3), "3", "1"}) + " " +
            joinWithSlash({to_string(v4), "4", "1"}) + " "
    );
}

void ObjParser::addPoly(const Polygon& poly) {
    string face;

    for (const auto &vertex: poly) {
        uint64_t vertex_idx = addVertex(vertex);
        face += joinWithSlash({to_string(vertex_idx), "1", "1"}) + " ";
    }
    faces.push_back(face);
}

string ObjParser::strFromVertex(Vertex vertex) {
    if (vertex.is_3d) return to_string(vertex.x) + " " + to_string(vertex.z) + " " + to_string(vertex.y);
    return to_string(vertex.x) + " " + to_string(vertex.y);
}

uint64_t ObjParser::addVertex(Vertex vertex) {
    string name = strFromVertex(vertex);
//    std::cout << name << " " << existing_vertices.contains(name) << " " << "\n";
    if (existing_vertices.contains(name)) {
//        std::cout << "   " << existing_vertices[name] << " " << "\n";
        return existing_vertices[name];
    }
    trig_vertices.push_back(vertex);
    uint64_t idx = trig_vertices.size();
    existing_vertices.insert({name, idx});
    return idx;
}

string ObjParser::joinWithSlash(vector<string> parts) {
    string res = parts[0];

    for (auto str_part = begin(parts) + 1; str_part != end(parts); ++str_part) {
        res += "/" + *str_part;
    }
    return res;
}

string ObjParser::toStr() {
    string res = "# Auto House (Rui Calsaverini)\n";
    res += "mtllib untitled.mtl\no AutoHouse\n";

    for (const auto &vertex: trig_vertices) {
        res += "v " + strFromVertex(vertex) + "\n";
    }
    for (const auto &vertex: uv_vertices) {
        res += "vt " + strFromVertex(vertex) + "\n";
    }
    for (const auto &vertex: normals) {
        res += "vn " + strFromVertex(vertex) + "\n";
    }
    for (const auto &face: faces) {
        res += "f " + face + "\n";
    }

    return res;
}

