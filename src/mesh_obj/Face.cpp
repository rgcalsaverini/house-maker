#include "Face.h"
#include <cmath>

using std::acos;
using std::cos;
using std::sin;
using std::max;

double distance(Vertex p1, Vertex p2) {
    double dx = p2.x - p1.x;
    double dy = p2.y - p1.y;
    double dz = p2.z - p1.z;

    return std::sqrt(dx * dx + dy * dy + dz * dz);
}

Face::Face(Vertex p1, Vertex p2, Vertex p3) {
    geometry_points = {
            p1.withZ(p1.z),
            p2.withZ(p2.z),
            p3.withZ(p3.z)
    };
    addTriangleUV();
}

Face::Face(Vertex p1, Vertex p2, Vertex p3, Vertex p4) {
    geometry_points = {
            p1.withZ(p1.z),
            p2.withZ(p2.z),
            p3.withZ(p3.z),
            p4.withZ(p4.z)
    };
    addRectangleUV();
}

void Face::shadeSmooth() {
    smooth = true;
}

void Face::shadeFlat() {
    smooth = false;
}

std::vector<Vertex> Face::vertices() const {
    return geometry_points;
}

bool Face::isSmooth() const {
    return smooth;
}


void Face::addTriangleUV() {
    double dist_ab = distance(geometry_points[0], geometry_points[1]);
    double dist_ac = distance(geometry_points[0], geometry_points[2]);
    double dist_bc = distance(geometry_points[1], geometry_points[2]);
    double angle_bac = acos((dist_ac * dist_ac + dist_ab * dist_ab - dist_bc * dist_bc) / (2 * dist_ac * dist_ab));
    Vertex pt1 = Vertex(0, 0);
    Vertex pt2 = Vertex(dist_ab * cos(angle_bac), dist_ab * sin(angle_bac));
    Vertex pt3 = Vertex(dist_ac, 0);
    uv_points = {pt1, pt2, pt3};
    uv_bounding_box.first = max(max(pt1.x, pt2.x), pt3.x);
    uv_bounding_box.second = max(max(pt1.y, pt2.y), pt3.y);
}

void Face::addRectangleUV() {
    double dist_ab = distance(geometry_points[0], geometry_points[1]);
    double dist_ad = distance(geometry_points[0], geometry_points[3]);
    double dist_bd = distance(geometry_points[1], geometry_points[3]);
    double dist_ac = distance(geometry_points[0], geometry_points[2]);
    double dist_cd = distance(geometry_points[2], geometry_points[3]);
    double angle_bad = acos((dist_ad * dist_ad + dist_ab * dist_ab - dist_bd * dist_bd) / (2 * dist_ad * dist_ab));
    double angle_cad = acos((dist_ad * dist_ad + dist_ac * dist_ac - dist_cd * dist_cd) / (2 * dist_ad * dist_ac));
    Vertex pt1 = Vertex(0, 0);
    Vertex pt2 = Vertex(dist_ab * cos(angle_bad), dist_ab * sin(angle_bad));
    Vertex pt3 = Vertex(dist_ac * cos(angle_cad), dist_ac * sin(angle_cad));
    Vertex pt4 = Vertex(dist_ad, 0);
    uv_points = {pt1, pt2, pt3, pt4};
    uv_bounding_box.first = max(max(pt1.x, pt2.x), max(pt4.x, pt3.x));
    uv_bounding_box.second = max(max(pt1.y, pt2.y), max(pt4.y, pt3.y));
}

std::vector<Vertex> Face::uv() const {
    return uv_points;
}

Face Face::verticalRectangle(Vertex base1, Vertex base2, float height) {
    Vertex edge_1 = base1.withZ(base1.z);
    Vertex edge_2 = base2.withZ(base2.z);
    Vertex top_1 = Vertex(edge_1.x, edge_1.y, base1.z + height);
    Vertex top_2 = Vertex(edge_2.x, edge_2.y, base2.z + height);
    return Face(edge_1, top_1, top_2, edge_2);
}

bool Face::isTriangular() const {
    return geometry_points.size() == 3;
}


