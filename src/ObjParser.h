// http://www.opengl-tutorial.org/beginners-tutorials/tutorial-7-model-loading/
#ifndef HOUSE_OBJPARSER_H
#define HOUSE_OBJPARSER_H

#include <vector>
#include <string>
#include <map>
#include <cstdint>
#include "Vertex.h"

using std::vector;
using std::string;
using std::map;

class ObjParser {
public:

public:
    void addVerticalPlane(Vertex p1, Vertex p2, float height);

    void addPoly(const Polygon& poly);

    uint64_t addVertex(Vertex vertex);

    string toStr();
private:
    static string strFromVertex(Vertex vertex);

    static string joinWithSlash(vector<string> parts);

private:
    vector<Vertex> trig_vertices;
    vector<string> faces;
    map<string, uint64_t> existing_vertices;

    /* Debug for now */
    vector<Vertex> uv_vertices = {
            Vertex(0, 0),
            Vertex(0, 1),
            Vertex(1, 1),
            Vertex(1, 0)
    };
    vector<Vertex> normals = {Vertex(0, 0, 1)};
};


#endif //HOUSE_OBJPARSER_H
