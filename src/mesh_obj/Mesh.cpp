#include "Mesh.h"

#include <utility>
#include <map>

using std::to_string;
using std::string;
using std::map;
using std::vector;

Mesh::Mesh(string obj_name) {
    name = std::move(obj_name);
    header = "# AutoHouse (Rui Calsaverini)";
}

void Mesh::add(const Face &face) {
    faces.push_back(face);
}

string Mesh::toObjString() {
    joinFacesVertices();
    separateFacesBySmoothness();
    preCalculateUVs();

    string output = header + "\n";
    output += "o " + name + "\n";

    for (const auto vertex: unique_vertices) {
        output += vertexToObjString("v", vertex);
    }
    for (const auto vertex: uvs) {
        output += vertexToObjString("vt", vertex);
    }
    output += "vn 0.000000 0.000000 0.000000\n";

    if (!flat_faces_idx.empty()) output += "s off\n";
    for (auto const idx: flat_faces_idx) output += faceToObjString(idx);
    if (!smooth_faces_idx.empty()) output += "s 1\n";
    for (auto const idx: smooth_faces_idx) output += faceToObjString(idx);

    return output;
}

void Mesh::joinFacesVertices() {
    map<uint64_t, uint64_t> vertex_to_idx;
    unique_vertices.clear();
    face_vertices_idx.clear();

    for (const auto &face: faces) {
        vector<uint64_t> new_face;
        for (const auto &vertex: face.vertices()) {
            uint64_t hash = vertexHash(vertex);
            if (vertex_to_idx.contains(hash)) {
                uint64_t vertex_idx = vertex_to_idx.at(hash);
                new_face.push_back(vertex_idx);
            } else {
                uint64_t idx = unique_vertices.size();
                unique_vertices.push_back(vertex);
                vertex_to_idx.insert({hash, idx});
                new_face.push_back(idx);
            }
        }
        face_vertices_idx.push_back(new_face);
    }
}

uint64_t Mesh::vertexHash(Vertex vertex) {
    // https://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function
    double pair_xy = 0.5 * (vertex.x + vertex.y) * (vertex.x + vertex.y + 1) + vertex.y;
    double pairing = 0.5 * (pair_xy + vertex.z) * (pair_xy + vertex.z + 1) + vertex.z;
    return (uint64_t) (pairing * 10000);
}

void Mesh::separateFacesBySmoothness() {
    uint64_t idx = 0;
    smooth_faces_idx.clear();
    flat_faces_idx.clear();

    for (const auto &face: faces) {
        if (face.isSmooth()) smooth_faces_idx.push_back(idx);
        else flat_faces_idx.push_back(idx);
        idx++;
    }
}


void Mesh::preCalculateUVs() {
    uvs.clear();
    face_uvs_idx.clear();
    for (const auto &face: faces) {
        vector<uint64_t> new_face_uv;
        for (const auto &uv: face.uv()) {
            new_face_uv.push_back(uvs.size());
            uvs.push_back(uv);
        }
        face_uvs_idx.push_back(new_face_uv);
    }
}

std::string Mesh::faceToObjString(uint64_t face_idx) {
    string output = "f ";

    for(uint64_t idx = 0 ; idx < faces[face_idx].vertices().size() ; idx++) {
        string geo = to_string(face_vertices_idx[face_idx][idx] + 1);
        string uv = to_string(face_uvs_idx[face_idx][idx] + 1);
        output += joinWithSlashes({geo, uv, "1"}) + " ";
    }
    return output + "\n";
}


string Mesh::vertexToObjString(const string& prefix, Vertex vertex) {
    string output = prefix + " ";
    if (vertex.is_3d) output += to_string(vertex.x) + " " + to_string(vertex.z) + " " + to_string(vertex.y);
    else output += to_string(vertex.x) + " " + to_string(vertex.y);
    return output + "\n";
}

std::string Mesh::joinWithSlashes(vector<string> parts) {
    string res = parts[0];

    for (auto str_part = begin(parts) + 1; str_part != end(parts); ++str_part) {
        res += "/" + *str_part;
    }
    return res;
}

