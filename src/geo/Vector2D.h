#ifndef HOUSE_VECTOR2D_H
#define HOUSE_VECTOR2D_H


class Vector2D {
public:
    Vector2D(float dir, float mag) : direction(dir), magnitude(mag) {}

public:
    float direction;
    float magnitude;
};


#endif //HOUSE_VECTOR2D_H
