#ifndef HOUSE_TILEROOF_H
#define HOUSE_TILEROOF_H


#include "../BaseTexture.h"

using std::pair;

class TileRoof : public BaseTexture {
public:
    TileRoof(int width, int height, float zoom = 1.0f, float aa = 2.0f);

    void generateTexture() override;

protected:
    bool drawTile(int row, int col, float height, float radius, Uint8 r, Uint8 g, Uint8 b);

protected:
    Sint16 cursor_x;

    int regular_width = 20;
    int regular_height = 25;
};


#endif //HOUSE_TILEROOF_H
