# House Maker

The goal of this project is to create fully randomized procedural houses to be used on 3D models and games
and eventually, create fully procedural cities.

This project is still in a very early stage. It is fully written in C++ and generated .obj 3D files and textures from
scratch.

This is a possible output:

![](./doc/screenshot.gif)

The textures are also fully procedural:

![](./doc/texture.png)

## Roadmap

### Stage 1
- [x] Generate a valid 3D model from scratch
- [x] Devise a way to represent multi-story structures
- [x] Add roofs
- [x] Generate a roof texture
- [x] Map UVs
- [ ] Use [this](https://github.com/juj/RectangleBinPack) to distribute the UV of the individual faces on the map
- [ ] Use SDL to resize and place the textures on the final map
- [ ] Fill roof gaps
- [ ] Generate texture normals

### Stage 2
- [ ] Add more textures (walls, more roofs...)
- [ ] Roof extend over walls
- [ ] Other roof shapes (mid-bend)
- [ ] Add windows and doors (both mesh and texture)
- [ ] Wall decorations (pillars, balconies)
- [ ] Roof Z axis extrusion
